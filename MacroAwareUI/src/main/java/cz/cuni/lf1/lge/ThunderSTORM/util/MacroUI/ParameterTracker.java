package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers.ComponentHandler;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers.ComponentHandlerMap;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.Validator;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;
import ij.Macro;
import ij.Prefs;
import ij.plugin.frame.Recorder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A class that handles input parameters for ImageJ plugins. It provides macro
 * support: parsing macro options, saving values as macro options. It does
 * reading values from UI components, validation of input values, reseting to
 * default values. Optionally it supports persistent saving and loading of
 * parameter values using ImageJ IJ.Prefs class.
 */
public class ParameterTracker {

    Map<ParameterKey, TrackedParameter> trackedParameters;
    transient String prefsPrefix;
    transient ComponentHandlerMap handlers;
    transient boolean noGuiParsAllowed = false;

    /**
     * Sets the prefix for saving parameter values to IJ.Prefs. Setting a null
     * value disables using persistent parameter values.
     */
    public void setPrefsPrefix(String prefsPrefix) {
        this.prefsPrefix = prefsPrefix;
    }

    public ParameterTracker() {
        this(null);
    }

    public ParameterTracker(String prefsPrefix) {
        this.prefsPrefix = prefsPrefix;
        trackedParameters = new HashMap<ParameterKey, TrackedParameter>();
        handlers = ComponentHandlerMap.getDefaultHandlers();
    }

    /**
     * Returns true if saving parameter values to IJ.Prefs is enabled. It can be
     * enabled by setting non-null prefix during construction or using
     * {@link ParameterTracker#setPrefsPrefix(java.lang.String)}.
     */
    public boolean isPrefsSavingEnabled() {
        return prefsPrefix != null;
    }

    /**
     * If set to true, Parameters with no component registered with them are
     * skipped silently when handling the state of GUI components. If false,
     * reading state from GUI fails with exception if some parameters have no
     * components registered. Defaults to false. Can be used to introduce
     * optional parameters that have no GUI and can only be changed in a macro.
     *
     * @param allowed
     */
    public void setNoGuiParametersAllowed(boolean allowed) {
        noGuiParsAllowed = allowed;
    }

    //------ create params ------
    /**
     * @see ParameterTracker#createDoubleField(String, Validator, double,
     * ParameterTracker.Condition)
     */
    public ParameterKey.Double createDoubleField(String keyString, Validator<Double> validator, double defaultValue) {
        return createDoubleField(keyString, validator, defaultValue, null);
    }

    /**
     * Creates a double parameter.
     *
     * @param keyString A string to identify the parameter. Its used as a key in
     * Macro options and in preferences.
     * @param validator An optional Validator that checks the correctness of the
     * value.
     * @param defaultValue
     * @param cond an optional Condition that can specify conditional processing
     * of the parameter. If the condition is not satisfied, the parameter is
     * ignored while loading/saving macro options, saving to preferences,
     * reading values from gui.
     * @return An identifier that is used to get the value of the parameter or
     * register gui components with it.
     */
    public ParameterKey.Double createDoubleField(String keyString, Validator<Double> validator, double defaultValue, Condition cond) {
        ParameterKey.Double key = new ParameterKey.Double(keyString, this);
        DoubleParameter par = new DoubleParameter();
        par.validator = validator;
        par.defaultValue = defaultValue;
        par.value = defaultValue;
        par.condition = cond;
        trackedParameters.put(key, par);
        return key;
    }

    /**
     * @see ParameterTracker#createIntField(String, Validator, int,
     * ParameterTracker.Condition)
     */
    public ParameterKey.Integer createIntField(String keyString, Validator<Integer> validator, int defaultValue) {
        return createIntField(keyString, validator, defaultValue, null);
    }

    /**
     * Creates an int parameter.
     *
     * @param keyString A string to identify the parameter. Its used as a key in
     * Macro options and in preferences.
     * @param validator An optional Validator that checks the correctness of the
     * value.
     * @param defaultValue
     * @param cond an optional Condition that can specify conditional processing
     * of the parameter. If the condition is not satisfied, the parameter is
     * ignored while loading/saving macro options, saving to preferences,
     * reading values from gui.
     * @return An identifier that is used to get the value of the parameter or
     * register gui components with it.
     */
    public ParameterKey.Integer createIntField(String keyString, Validator<Integer> validator, int defaultValue, Condition cond) {
        ParameterKey.Integer key = new ParameterKey.Integer(keyString, this);
        IntParameter par = new IntParameter();
        par.validator = validator;
        par.defaultValue = defaultValue;
        par.value = defaultValue;
        par.condition = cond;
        trackedParameters.put(key, par);
        return key;
    }

    /**
     * @see ParameterTracker#createStringField(String, Validator, String,
     * ParameterTracker.Condition)
     */
    public ParameterKey.String createStringField(String keyString, Validator<String> validator, String defaultValue) {
        return createStringField(keyString, validator, defaultValue, null);
    }

    /**
     * Creates a String parameter.
     *
     * @param keyString A string to identify the parameter. Its used as a key in
     * Macro options and in preferences.
     * @param validator An optional Validator that checks the correctness of the
     * value.
     * @param defaultValue
     * @param cond an optional Condition that can specify conditional processing
     * of the parameter. If the condition is not satisfied, the parameter is
     * ignored while loading/saving macro options, saving to preferences,
     * reading values from gui.
     * @return An identifier that is used to get the value of the parameter or
     * register gui components with it.
     */
    public ParameterKey.String createStringField(String keyString, Validator<String> validator, String defaultValue, Condition cond) {
        ParameterKey.String key = new ParameterKey.String(keyString, this);
        StringParameter par = new StringParameter();
        par.validator = validator;
        par.defaultValue = defaultValue;
        par.value = defaultValue;
        par.condition = cond;
        trackedParameters.put(key, par);
        return key;
    }

    /**
     * @see ParameterTracker#createBooleanField(String, Validator, boolean,
     * ParameterTracker.Condition)
     */
    public ParameterKey.Boolean createBooleanField(String keyString, Validator<Boolean> validator, boolean defaultValue) {
        return createBooleanField(keyString, validator, defaultValue, null);
    }

    /**
     * Creates a boolean parameter.
     *
     * @param keyString A string to identify the parameter. Its used as a key in
     * Macro options and in preferences.
     * @param validator An optional Validator that checks the correctness of the
     * value.
     * @param defaultValue
     * @param cond an optional Condition that can specify conditional processing
     * of the parameter. If the condition is not satisfied, the parameter is
     * ignored while loading/saving macro options, saving to preferences,
     * reading values from gui.
     * @return An identifier that is used to get the value of the parameter or
     * register gui components with it.
     */
    public ParameterKey.Boolean createBooleanField(String keyString, Validator<Boolean> validator, boolean defaultValue, Condition cond) {
        ParameterKey.Boolean key = new ParameterKey.Boolean(keyString, this);
        BooleanParameter par = new BooleanParameter();
        par.validator = validator;
        par.defaultValue = defaultValue;
        par.value = defaultValue;
        par.condition = cond;
        trackedParameters.put(key, par);
        return key;
    }

    //------ get param values ------
    /**
     * Returns the value of the parameter. The value can be the default value, a
     * value loaded from preferences,a value parsed from macro or a value read
     * from a dialog gui component.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @throws IllegalArgumentException if the parameter does not exist.
     */
    public double getDouble(ParameterKey.Double key) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof DoubleParameter) {
            DoubleParameter dpar = (DoubleParameter) par;
            return dpar.value;
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a double parameter.");
        }
    }

    /**
     * Sets the value of a parameter and updates the UI.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @param value
     * @throws IllegalArgumentException if the parameter does not exist or if it
     * is not a double parameter
     */
    public void setDouble(ParameterKey.Double key, double value) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof DoubleParameter) {
            DoubleParameter dpar = (DoubleParameter) par;
            dpar.value = value;
            if (par.component != null || !noGuiParsAllowed) {
                par.saveStateToComponent();
            }
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a double parameter.");
        }
    }

    /**
     * Returns the value of the parameter. The value can be the default value, a
     * value loaded from preferences,a value parsed from macro or a value read
     * from a dialog gui component.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @throws IllegalArgumentException if the parameter does not exist.
     */
    public int getInt(ParameterKey.Integer key) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof IntParameter) {
            IntParameter ipar = (IntParameter) par;
            return ipar.value;
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not an int parameter.");
        }
    }

    /**
     * Sets the value of a parameter and updates the UI.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @param value
     * @throws IllegalArgumentException if the parameter does not exist or if it
     * is not an int parameter
     */
    public void setInt(ParameterKey.Integer key, int value) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof IntParameter) {
            IntParameter ipar = (IntParameter) par;
            ipar.value = value;
            if (par.component != null || !noGuiParsAllowed) {
                par.saveStateToComponent();
            }
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not an int parameter.");
        }
    }

    /**
     * Returns the value of the parameter. The value can be the default value, a
     * value loaded from preferences,a value parsed from macro or a value read
     * from a dialog gui component.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @throws IllegalArgumentException if the parameter does not exist.
     */
    public String getString(ParameterKey.String key) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof StringParameter) {
            StringParameter spar = (StringParameter) par;
            return spar.value;
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a String parameter.");
        }
    }

    /**
     * Sets the value of a parameter and updates the UI.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @param value
     * @throws IllegalArgumentException if the parameter does not exist or if it
     * is not a String parameter
     */
    public void setString(ParameterKey.String key, String value) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof StringParameter) {
            StringParameter spar = (StringParameter) par;
            spar.value = value;
            if (par.component != null || !noGuiParsAllowed) {
                par.saveStateToComponent();
            }
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a String parameter.");
        }
    }

    /**
     * Returns the value of the parameter. The value can be the default value, a
     * value loaded from preferences,a value parsed from macro or a value read
     * from a dialog gui component.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @throws IllegalArgumentException if the parameter does not exist.
     */
    public boolean getBoolean(ParameterKey.Boolean key) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof BooleanParameter) {
            BooleanParameter spar = (BooleanParameter) par;
            return spar.value;
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a boolean parameter.");
        }
    }

    /**
     * Sets the value of a parameter and updates the UI.
     *
     * @param key ParameterKey returned by one of the create*(...) methods
     * @param value
     * @throws IllegalArgumentException if the parameter does not exist or if it
     * is not a Boolean parameter
     */
    public void setBoolean(ParameterKey.Boolean key, boolean value) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        if (par instanceof BooleanParameter) {
            BooleanParameter spar = (BooleanParameter) par;
            spar.value = value;
            if (par.component != null || !noGuiParsAllowed) {
                par.saveStateToComponent();
            }
        } else {
            throw new IllegalArgumentException("Parameter " + key + " is not a boolean parameter.");
        }
    }

    //------ component registration ------
    /**
     * Registers a GUI component with the specified parameter. Parameter values
     * are read from the component when
     * {@link ParameterTracker#readDialogOptions()} is called. The components
     * state is updated when {@link ParameterTracker#resetToDefaults(boolean)}
     * is called. Only some types of components are supported for each parameter
     * type.
     *
     * The component is returned to allow for method chaining. The currently
     * supported components are:
     * <ul>
     * <li>Double parameters: JTextComponent</li>
     *
     * <li>Integer parameters: JTextComponent</li>
     *
     * <li>Boolean parameters: JTextComponent</li>
     *
     * <li>String parameters: JTextComponent, JComboBox, JTabbedPane,
     * ButtonGroup</li>
     * </ul>
     * Support for more GUI components can be added by creating a
     * {@link ComponentHandler} for the component and adding it to the known
     * handlers (see {@link ParameterTracker#getComponentHandlers()}).
     *
     * @param <T>
     * @param key the parameter to which the component is registered
     * @param component the registered component
     * @return component
     */
    public <T> T registerComponent(ParameterKey key, T component) {
        TrackedParameter par = trackedParameters.get(key);
        par.setComponent(component);
        par.handler = handlers.getComponentHandler(key.getClass(), component);
        if (par.handler == null) {
            throw new IllegalArgumentException("No component handler for this component class: " + component.getClass() + ".");
        }
        return component;
    }

    /**
     * Returns a component that was previously registered with the parameter by
     * a call to {@code registerComponent(...)}, or null if no component was
     * registered.
     */
    public Object getRegisteredComponent(ParameterKey key) {
        TrackedParameter par = trackedParameters.get(key);
        if (par == null) {
            throw new IllegalArgumentException("No parameter " + key + ".");
        }
        return par.getComponent();
    }

    /**
     * Returns a map of handlers that are used to read parameter values from
     * (and save them to) the state of various GUI components.
     *
     */
    public ComponentHandlerMap getComponentHandlers() {
        return handlers;
    }

    //------ macro ------
    /**
     * Records parameter values as macro options. Parameters that have a
     * {@link Condition} and it evaluates to false are skipped.
     */
    public void recordMacroOptions() {
        if (Recorder.record) {
            for (Map.Entry<ParameterKey, TrackedParameter> entry : trackedParameters.entrySet()) {
                TrackedParameter par = entry.getValue();
                if (par.condition == null || par.condition.isSatisfied()) {
                    Recorder.recordOption(entry.getKey().toString(), escapeString(entry.getValue().dumpState()));
                }
            }
        }
    }

    /**
     * Parses parameter values from macro options. Parameters that have a
     * validator attached are validated. Parameters that have a
     * {@link Condition} and it evaluates to false are skipped.
     *
     * @throws ValidatorException if validation fails
     */
    public void readMacroOptions() {
        assert Macro.getOptions() != null;
        for (Map.Entry<ParameterKey, TrackedParameter> entry : trackedParameters.entrySet()) {
            readOneParMacroOptions(entry.getKey(), entry.getValue());
        }
    }

    private void readOneParMacroOptions(ParameterKey key, TrackedParameter par) {
        if (par.condition != null) {
            //process dependent parameters first
            ParameterKey[] dependsOn = par.condition.dependsOn();
            if (dependsOn != null) {
                for (ParameterKey dependentKey : dependsOn) {
                    TrackedParameter dependent = trackedParameters.get(dependentKey);
                    if (dependent != null) {
                        readOneParMacroOptions(dependentKey, dependent);
                    }
                }
            }
        }
        if (par.condition == null || par.condition.isSatisfied()) {
            String options = Macro.getOptions();
            par.parseState(Macro.getValue(options, trimKey(key.toString()), par.dumpDefaultState()));
            par.validate();
        }
    }

    //------ dialog ------
    /**
     * Reads parameter values from registered components. Every parameter must
     * have a component registered to it (using {@code registerComponent}).
     * Parameters that have a validator attached are validated. Parameters that
     * have a {@link Condition} and it evaluates to false are skipped.
     *
     * @throws ValidatorException if validation fails
     */
    public void readDialogOptions() {
        for (TrackedParameter par : trackedParameters.values()) {
            readOneParDialogOptions(par);
        }
    }

    private void readOneParDialogOptions(TrackedParameter par) {
        if (par.condition != null) {
            //process dependent parameters first
            ParameterKey[] dependsOn = par.condition.dependsOn();
            if (dependsOn != null) {
                for (ParameterKey dependentKey : dependsOn) {
                    TrackedParameter dependent = trackedParameters.get(dependentKey);
                    if (dependent != null) {
                        readOneParDialogOptions(dependent);
                    }
                }
            }
        }
        if (par.condition == null || par.condition.isSatisfied()) {
            if (par.component != null || !noGuiParsAllowed) {
                par.readStateFromComponent();
                par.validate();
            }
        }
    }

    /**
     * Update the state of registered components to reflect current values.
     */
    public void updateComponents() {
        for (TrackedParameter par : trackedParameters.values()) {
            if (par.component != null || !noGuiParsAllowed) {
                par.saveStateToComponent();
            }
        }
    }

    /**
     * Resets the parameters to their default values. Registered components are
     * updated.
     *
     * @param updateComponents
     */
    public void resetToDefaults(boolean updateComponents) {
        for (TrackedParameter par : trackedParameters.values()) {
            par.parseState(par.dumpDefaultState());
            if (updateComponents) {
                if (par.component != null || !noGuiParsAllowed) {
                    par.saveStateToComponent();
                }
            }
        }
    }

    /**
     * Validates parameters that have a {@link Validator} attached.
     *
     * @throws ValidatorException if validation fails
     */
    public void validate() {
        for (TrackedParameter par : trackedParameters.values()) {
            if (par.condition == null || par.condition.isSatisfied()) {
                par.validate();
            }
        }
    }

    //------ persistent preferences -------
    /**
     * Loads parameter values from a persistent location. The file is
     * {@code IJ_prefs.txt} and its handled by the ImageJ class IJ.Prefs.
     * Default value is used for every parameter for which there is no value
     * saved (or the value is invalid). Preferences prefix must have been set to
     * a non-null value in constructor or using
     * {@link ParameterTracker#setPrefsPrefix(java.lang.String)}.
     */
    public void loadPrefs() {
        if (!isPrefsSavingEnabled()) {
            throw new RuntimeException("Preferences use is not enabled. Set preferences prefix to enable it.");
        }
        for (Map.Entry<ParameterKey, TrackedParameter> entry : trackedParameters.entrySet()) {
            String key = entry.getKey().toString();
            TrackedParameter par = entry.getValue();
            String pref = Prefs.get(prefsPrefix + "." + key, par.dumpDefaultState());
            try {
                par.parseState(pref);
                par.validate();
            } catch (Exception ex) {
                par.parseState(par.dumpDefaultState());
            }
            if (par.getComponent() != null) {
                par.saveStateToComponent();
            }
        }
    }

    /**
     * Saves parameter values to a persistent location. The file is
     * {@code IJ_prefs.txt} and its handled by the ImageJ class IJ.Prefs.
     * Preferences prefix must have been set to a non-null value in constructor
     * or using {@link ParameterTracker#setPrefsPrefix(java.lang.String)}.
     */
    public void savePrefs() {
        if (!isPrefsSavingEnabled()) {
            throw new RuntimeException("Preferences use is not enabled. Set preferences prefix to enable it.");
        }
        for (Map.Entry<ParameterKey, TrackedParameter> entry : trackedParameters.entrySet()) {
            TrackedParameter trackedParameter = entry.getValue();
            if (trackedParameter.condition == null || trackedParameter.condition.isSatisfied()) {
                Prefs.set(prefsPrefix + "." + entry.getKey(), trackedParameter.dumpState());
            }
        }
    }

    //macro helpers
    private String escapeString(String string) {
        return string.replace("\\", "\\\\");
    }

    static String trimKey(String key) {
        int index = key.indexOf(" ");
        if (index > -1) {
            key = key.substring(0, index);
        }
        index = key.indexOf(":");
        if (index > -1) {
            key = key.substring(0, index);
        }
        key = key.toLowerCase(Locale.US);
        return key;
    }

    /**
     * A condition object that can be used to disable processing of some
     * parameter based on a user defined criteria.
     * <p>
     * For example parsing of a parameter can be done only when another boolean
     * parameter is true.
     * </p>
     */
    public interface Condition {

        /**
         * Called before processing a parameter (parsing from macro options,
         * reading from dialog components, saving to prefs, recording macro
         * options). If the returned value is false the processing of the
         * parameter is skipped.
         *
         * @return {@code true} if the parameter should be processed,
         * {@code false} otherwise
         */
        public boolean isSatisfied();

        /**
         * Specifies which parameters must be processed before evaluating this
         * condition. ParameterTracker ensures that parameters returned in this
         * function will be done processing before calling the isSatisfied
         * method.
         */
        public ParameterKey[] dependsOn();
    }
}
