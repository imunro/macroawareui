package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers.ComponentHandler;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.Validator;
import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
abstract class TrackedParameter<T> {

    T value;
    transient T defaultValue;
    transient Validator<T> validator;
    transient Object component;
    transient ComponentHandler<T> handler;
    transient ParameterTracker.Condition condition;

    void setComponent(Object comp) {
        component = comp;
    }

    abstract String dumpState();

    abstract String dumpDefaultState();

    abstract void parseState(String dump);

    void readStateFromComponent() {
        if (component == null) {
            throw new NullPointerException("No component was registered for this parameter.");
        }
        value = handler.getValueFromComponent(component);
    }

    void saveStateToComponent() {
        if (component == null) {
            throw new NullPointerException("No component was registered for this parameter.");
        }
        handler.setValueToComponent(value, component);
    }

    void validate() throws ValidatorException {
        if (validator != null) {
            try {
                validator.validate(value);
            } catch (ValidatorException validatorException) {
                validatorException.setSource(component);
                throw validatorException;
            }
        }
    }

    Object getComponent() {
        return component;
    }
}

class BooleanParameter extends TrackedParameter<Boolean> {

    @Override
    String dumpState() {
        return value + "";
    }

    @Override
    String dumpDefaultState() {
        return defaultValue + "";
    }

    @Override
    void parseState(String dump) {
        value = Boolean.parseBoolean(dump);
    }
}

class DoubleParameter extends TrackedParameter<Double> {

    @Override
    String dumpState() {
        return "" + value;
    }

    @Override
    void parseState(String dump) {
        try {
            value = Double.parseDouble(dump);
        } catch (NumberFormatException e) {
            throw new ValidatorException("The input string \"" + dump + "\" cannot be converted to double.", e, component);
        }
    }

    @Override
    String dumpDefaultState() {
        return "" + defaultValue;
    }
}

class IntParameter extends TrackedParameter<Integer> {

    @Override
    String dumpState() {
        return value + "";
    }

    @Override
    void parseState(String dump) {
        try {
            value = Integer.parseInt(dump);
        } catch (NumberFormatException e) {
            throw new ValidatorException("The input string \"" + dump + "\" cannot be converted to int.", e, component);
        }
    }

    @Override
    String dumpDefaultState() {
        return "" + defaultValue;
    }
}

class StringParameter extends TrackedParameter<String> {

    String dumpState() {
        return value;
    }

    @Override
    String dumpDefaultState() {
        return defaultValue;
    }

    @Override
    void parseState(String dump) {
        value = dump;
    }
}
