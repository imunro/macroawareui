package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.ParameterKey;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

/**
 * A class that holds known {@link ComponentHandler}s for various components.
 * Each parameter type (ParameterKey subclass) has its own set of handlers. It
 * is basically a Map from component class to a ComponentHandler for each of the
 * parameter types. New ComponentHandlers can be added to support reading/saving
 * of parameters from various gui components.
 *
 * @author Josef Borkovec
 */
public class ComponentHandlerMap {

    Map<Class<? extends ParameterKey>, Map<Class<?>, ComponentHandler>> handlers;

    ComponentHandlerMap() {
        handlers = new HashMap<Class<? extends ParameterKey>, Map<Class<?>, ComponentHandler>>(4);
        handlers.put(ParameterKey.Boolean.class, new HashMap<Class<?>, ComponentHandler>());
        handlers.put(ParameterKey.String.class, new HashMap<Class<?>, ComponentHandler>());
        handlers.put(ParameterKey.Double.class, new HashMap<Class<?>, ComponentHandler>());
        handlers.put(ParameterKey.Integer.class, new HashMap<Class<?>, ComponentHandler>());
    }

    /**
     * Returns a ComponentHandlerMap filled with default handlers.
     */
    public static ComponentHandlerMap getDefaultHandlers() {
        ComponentHandlerMap map = new ComponentHandlerMap();
        map.addForDoubleParameters(JTextComponent.class, new JTextComponentToDoubleHandler());

        map.addForIntegerParameters(JTextComponent.class, new JTextComponentToIntegerHandler());

        map.addForBooleanParameters(JToggleButton.class, new JToggleButtonToBooleanHandler());

        map.addForStringParameters(JTextComponent.class, new JTextComponentToStringHandler());
        map.addForStringParameters(JComboBox.class, new JComboBoxToStringHandler());
        map.addForStringParameters(JTabbedPane.class, new JTabbedPaneToStringHandler());
        map.addForStringParameters(ButtonGroup.class, new ButtonGroupToStringHandler());

        return map;
    }

    /**
     * Returns a component handler for the specified component and parameter
     * type.
     */
    public ComponentHandler<String> getComponentHandler(Class<? extends ParameterKey> keyClass, Object component) {
        assert keyClass != null;
        assert component != null;

        Map<Class<?>, ComponentHandler> mapForParameterType = handlers.get(keyClass);
        if (mapForParameterType == null) {
            return null;
        }
        Class c = component.getClass();
        do {
            ComponentHandler handler = mapForParameterType.get(c);
            if (handler != null) {
                return handler;
            }
            c = c.getSuperclass();
        } while (c != null);
        return null;
    }

    /**
     * Adds a new ComponentHandler for String parameters.
     */
    public void addForStringParameters(Class<?> componentClass, ComponentHandler<String> handler) {
        add(componentClass, handler, ParameterKey.String.class);
    }

    /**
     * Adds a new ComponentHandler for Boolean parameters.
     */
    public void addForBooleanParameters(Class<?> componentClass, ComponentHandler<Boolean> handler) {
        add(componentClass, handler, ParameterKey.Boolean.class);
    }

    /**
     * Adds a new ComponentHandler for Integer parameters.
     */
    public void addForIntegerParameters(Class<?> componentClass, ComponentHandler<Integer> handler) {
        add(componentClass, handler, ParameterKey.Integer.class);
    }

    /**
     * Adds a new ComponentHandler for Double parameters.
     */
    public void addForDoubleParameters(Class<?> componentClass, ComponentHandler<Double> handler) {
        add(componentClass, handler, ParameterKey.Double.class);
    }

    private void add(Class<?> componentClass, ComponentHandler handler, Class<? extends ParameterKey> parameterClass) {
        assert componentClass != null;
        assert handler != null;

        Map<Class<?>, ComponentHandler> mapForParameterType = handlers.get(parameterClass);
        assert mapForParameterType != null;
        mapForParameterType.put(componentClass, handler);
    }

}
