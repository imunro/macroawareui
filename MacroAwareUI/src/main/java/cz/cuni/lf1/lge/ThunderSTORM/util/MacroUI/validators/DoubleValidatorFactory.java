package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

/**
 * Factory that provides several validators for double parameters.
 */
public class DoubleValidatorFactory {

    /**
     * A validator that checks if a value is in range. Border values included.
     */
    public static Validator<Double> rangeInclusive(final double low, final double high) {
        return new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                checkLowerBoundInclusive(low, input);
                checkUpperBoundInclusive(high, input);
            }
        };
    }

    /**
     * A validator that checks if a value is in range. Border values are not
     * included.
     */
    public static Validator<Double> rangeExclusive(final double low, final double high) {
        return new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                checkLowerBoundExclusive(low, input);
                checkUpperBoundExclusive(high, input);
            }
        };
    }

    /**
     * A validator that checks if a value is positive. Zero included.
     */
    public static Validator<Double> positive() {
        return new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                checkLowerBoundInclusive(0, input);
            }
        };
    }

    /**
     * A validator that checks if a value is positive. Zero not included.
     */
    public static Validator<Double> positiveNonZero() {
        return new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                checkLowerBoundExclusive(0, input);
            }
        };
    }

    /**
     * A validator that checks if a value is negative. Zero included.
     */
    public static Validator<Double> negative() {
        return new Validator<Double>() {
            public void validate(Double input) throws ValidatorException {
                checkUpperBoundInclusive(0, input);
            }
        };
    }

    static void checkLowerBoundInclusive(double bound, double value) {
        if (value < bound) {
            throw new ValidatorException("Input value must be greater than or equal to " + bound + ". Input value: " + value);
        }
    }

    static void checkLowerBoundExclusive(double bound, double value) {
        if (value <= bound) {
            throw new ValidatorException("Input value must be greater than " + bound + ". Input value: " + value);
        }
    }

    static void checkUpperBoundInclusive(double bound, double value) {
        if (value > bound) {
            throw new ValidatorException("Input value must be smaller than or equal to " + bound + ". Input value: " + value);
        }
    }

    static void checkUpperBoundExclusive(double bound, double value) {
        if (value >= bound) {
            throw new ValidatorException("Input value must be smaller than " + bound + ". Input value: " + value);
        }
    }
}
