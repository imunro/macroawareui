package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

/**
 * A class that joins multiple Validators into one.
 */
public class CompoundValidator<T> implements Validator<T> {

    Validator<T>[] validators;

    public CompoundValidator(Validator<T>... validators) {
        this.validators = validators;
    }

    public void validate(T input) throws ValidatorException {
        for (Validator<T> validator : validators) {
            validator.validate(input);
        }
    }
}
