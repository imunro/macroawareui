package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.Utils;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class ButtonGroupToStringHandler implements ComponentHandler<String> {

    public String getValueFromComponent(Object comp) {
        assert (comp instanceof ButtonGroup);
        ButtonGroup bg = (ButtonGroup) comp;

        Enumeration<AbstractButton> buttons = bg.getElements();
        while (buttons.hasMoreElements()) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return button.getText();
            }
        }
        return null;
    }

    public void setValueToComponent(String value, Object comp) {
        assert (comp instanceof ButtonGroup);
        ButtonGroup bg = (ButtonGroup) comp;

        Enumeration<AbstractButton> buttons = bg.getElements();
        while (buttons.hasMoreElements()) {
            AbstractButton button = buttons.nextElement();
            if (button.getText().equals(value)) {
                if (!button.isSelected()) {
                    Utils.click(button);
                    break;
                }
            }
        }
    }
}
