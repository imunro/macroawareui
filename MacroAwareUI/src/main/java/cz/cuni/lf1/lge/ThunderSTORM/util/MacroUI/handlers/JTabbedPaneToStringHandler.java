package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import javax.swing.JTabbedPane;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JTabbedPaneToStringHandler implements ComponentHandler<String> {

    public String getValueFromComponent(Object comp) {
        assert (comp instanceof JTabbedPane);
        JTabbedPane tabs = (JTabbedPane) comp;

        return tabs.getTitleAt(tabs.getSelectedIndex());
    }

    public void setValueToComponent(String value, Object comp) {
        assert (comp instanceof JTabbedPane);
        JTabbedPane tabs = (JTabbedPane) comp;

        int index = 0;
        for (int i = 0; i < tabs.getTabCount(); i++) {
            if (tabs.getTitleAt(i).equals(value)) {
                index = i;
                break;
            }
        }
        tabs.setSelectedIndex(index);
    }
}
