package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators;

/**
 * Exception to indicate that validation of input parameter failed. The
 * component whose input validation failed can be specified.
 */
public class ValidatorException extends RuntimeException {

    Object source;

    public ValidatorException() {
    }

    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(String message, Throwable cause, Object source) {
        super(message, cause);
        this.source = source;
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getSource() {
        return source;
    }
}
