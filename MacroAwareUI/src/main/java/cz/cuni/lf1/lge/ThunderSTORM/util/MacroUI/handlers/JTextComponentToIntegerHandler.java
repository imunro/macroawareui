package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.handlers;

import cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI.validators.ValidatorException;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
class JTextComponentToIntegerHandler implements ComponentHandler<Integer> {

    public Integer getValueFromComponent(Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        try {
            return Integer.parseInt(textField.getText());
        } catch (NumberFormatException e) {
            throw new ValidatorException("The input string \"" + textField.getText() + "\" cannot be converted to double.", e, textField);
        }
    }

    public void setValueToComponent(Integer value, Object comp) {
        assert (comp instanceof JTextComponent);
        JTextComponent textField = (JTextComponent) comp;
        textField.setText(value + "");
    }
}
