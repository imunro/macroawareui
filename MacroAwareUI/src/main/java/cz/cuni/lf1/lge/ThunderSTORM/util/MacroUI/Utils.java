package cz.cuni.lf1.lge.ThunderSTORM.util.MacroUI;

import ij.WindowManager;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;

/**
 *
 */
public class Utils {

    /**
     * Returns an array with titles of images currently open by ImageJ.
     * Returns null if no images are open and addEmptyString is false.
     * @param addEmptyString when true adds an empty String to the list
     */
    public static String[] getOpenImageTitles(boolean addEmptyString) {
        int[] imageIDs = WindowManager.getIDList();
        if (imageIDs != null) {
            String[] possibleValues;
            if (addEmptyString) {
                possibleValues = new String[imageIDs.length + 1];
                possibleValues[possibleValues.length - 1] = "";
            } else {
                possibleValues = new String[imageIDs.length];
            }
            for (int i = 0; i < imageIDs.length; i++) {
                int id = imageIDs[i];
                possibleValues[i] = WindowManager.getImage(id).getTitle();
            }
            return possibleValues;
        } else {
            return addEmptyString ? new String[]{""} : null;
        }
    }

    /**
     * A method that does the same as {@link AbstractButton#doClick()} but
     * without calling Thread.sleep();
     */
    public static void click(AbstractButton btn) {
        ButtonModel model = btn.getModel();
        model.setArmed(true);
        model.setPressed(true);
        model.setPressed(false);
        model.setArmed(false);
    }
}
